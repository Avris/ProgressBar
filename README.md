# Code Doodle: a gradient progress bar

 - https://progressbar.avris.it
 - https://avris.it/projects/code-doodle-gradient-progress-bar

## Usage

Installation:

```bash
make install
```

Dev server:

```bash
make run
```

Build for production

```bash
make deploy
```

## Copyright

* **Author:** Andrea Vos [(avris.it)](https://avris.it)
* **Licence:** [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
